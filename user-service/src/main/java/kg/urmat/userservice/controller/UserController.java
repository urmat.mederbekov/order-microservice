package kg.urmat.userservice.controller;

import kg.urmat.userservice.facade.CartFacade;
import kg.urmat.userservice.facade.OrderFacade;
import kg.urmat.userservice.facade.UserFacade;
import kg.urmat.order_lib.dto.cart.CartResponse;
import kg.urmat.order_lib.dto.order.OrderResponse;
import kg.urmat.order_lib.dto.user.UserCreateRequest;
import kg.urmat.order_lib.dto.user.UserResponse;
import kg.urmat.order_lib.dto.user.UserUpdateRequest;
import kg.urmat.order_lib.util.contstants.RoleConstant;
import kg.urmat.order_lib.util.contstants.Route;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Route.USERS_API)
@Secured(RoleConstant.ADMIN)
@RequiredArgsConstructor
public class UserController {

    private final UserFacade userFacade;
    private final CartFacade cartFacade;
    private final OrderFacade orderFacade;

    @PreAuthorize("@principalContextProviderImpl.isAllowed(#id)")
    @GetMapping("/{id}")
    public UserResponse get(@PathVariable String id){
        return userFacade.get(id);
    }

    @PostMapping
    public UserResponse create(@RequestBody UserCreateRequest request){
        return userFacade.add(request);
    }

    @PutMapping("/{id}")
    public UserResponse update(@RequestBody UserUpdateRequest request, @PathVariable String id){
        return userFacade.update(request, id);
    }

    @PreAuthorize("@principalContextProviderImpl.isAllowed(#id)")
    @GetMapping("/{id}/cart")
    public CartResponse getCart(@PathVariable String id){
        return cartFacade.getByUserId(id);
    }

    @PreAuthorize("@principalContextProviderImpl.isAllowed(#id)")
    @GetMapping("/{id}/orders")
    public List<OrderResponse> getOrders(@PathVariable String id){return orderFacade.getAllByUserId(id);}
}
