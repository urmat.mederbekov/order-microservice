package kg.urmat.userservice.service.impl;

import kg.urmat.userservice.model.Role;
import kg.urmat.userservice.repository.RoleRepository;
import kg.urmat.userservice.service.RoleService;
import kg.urmat.order_lib.exception.NoSuchElementFoundException;
import kg.urmat.order_lib.util.enums.RoleCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;


@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    @Override
    public Role get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NoSuchElementFoundException(String.format("No Role with id %d found", id)));
    }

    @Override
    public Role getByCode(RoleCode code) {
        return repository.findByCode(code)
                .orElseThrow(() -> new NoSuchElementFoundException(String.format("No Role with code %s found", code)));
    }

    @Override
    public Set<Role> getAllByCodes(Collection<RoleCode> codes) {
        return repository.findByCodeIn(codes);
    }
}
