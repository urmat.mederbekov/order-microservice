package kg.urmat.userservice.service;

import kg.urmat.userservice.model.Role;
import kg.urmat.order_lib.util.enums.RoleCode;

import java.util.Collection;
import java.util.Set;

public interface RoleService {

    Role get(Long id);

    Set<Role> getAllByCodes(Collection<RoleCode> roles);

    Role getByCode(RoleCode code);
}
