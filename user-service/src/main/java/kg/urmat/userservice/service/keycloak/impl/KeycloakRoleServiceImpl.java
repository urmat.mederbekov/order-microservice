package kg.urmat.userservice.service.keycloak.impl;

import kg.urmat.userservice.service.keycloak.KeycloakRoleService;
import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KeycloakRoleServiceImpl implements KeycloakRoleService {

    @Value("${keycloak.realm}")
    private String realm;
    private final Keycloak keycloak;

    @Override
    public RoleRepresentation getByName(String roleCode) {
        return keycloak
                .realm(realm)
                .roles()
                .get(roleCode)
                .toRepresentation();
    }
}
