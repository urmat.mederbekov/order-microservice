package kg.urmat.userservice.service.keycloak.impl;

import kg.urmat.userservice.service.keycloak.KeycloakUserService;
import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class KeycloakUserServiceImpl implements KeycloakUserService {

    @Value("${keycloak.realm}")
    private String realm;
    private final Keycloak keycloak;

    @Override
    public Integer add(UserRepresentation user) {
        try (var response = keycloak.realm(realm).users().create(user)) {
            return response.getStatus();
        }
    }

    @Override
    public void update(UserRepresentation user) {
        keycloak
                .realm(realm)
                .users()
                .get(user.getId())
                .update(user);
    }

    @Override
    public List<UserRepresentation> getByUsername(String username) {
        return keycloak
                .realm(realm)
                .users()
                .search(username, true);
    }

    @Override
    public UserRepresentation getById(String id) {
        try {
            return keycloak
                    .realm(realm)
                    .users()
                    .get(id)
                    .toRepresentation();
        } catch (Exception e){
            throw new RuntimeException(String.format("User with id %s not found", id));
        }
    }

    @Override
    public void assignRoles(String id, List<RoleRepresentation> roles) {
         keycloak
                .realm(realm)
                .users()
                .get(id)
                .roles()
                .realmLevel()
                .add(roles);
    }
}
