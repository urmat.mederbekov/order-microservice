package kg.urmat.userservice.service.keycloak;

import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.List;

public interface KeycloakUserService {

    Integer add(UserRepresentation user);

    void update(UserRepresentation user);

    List<UserRepresentation> getByUsername(String username);

    UserRepresentation getById(String id);

    void assignRoles(String id, List<RoleRepresentation> roles);
}
