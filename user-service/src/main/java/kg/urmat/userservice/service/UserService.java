package kg.urmat.userservice.service;

import kg.urmat.userservice.model.User;

public interface UserService {

    User get(String authId);

    User getByUsername(String username);

    User save(User user);

    boolean existsByUsername(String username);
}
