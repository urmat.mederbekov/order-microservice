package kg.urmat.userservice.service.impl;

import kg.urmat.userservice.model.User;
import kg.urmat.userservice.repository.UserRepository;
import kg.urmat.userservice.service.UserService;
import kg.urmat.order_lib.exception.NoSuchElementFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Override
    public User get(String authId) {
        return repository.findByAuthId(authId)
                .orElseThrow(() -> new NoSuchElementFoundException(String.format("No User with authId %s found", authId)));
    }

    @Override
    public User getByUsername(String username) {
        return repository.findByUsername(username)
                .orElseThrow(() -> new NoSuchElementFoundException(String.format("No User with login %s found", username)));
    }

    @Override
    public User save(User user) {
        return repository.save(user);
    }

    @Override
    public boolean existsByUsername(String username) {
        return repository.existsByUsername(username);
    }
}
