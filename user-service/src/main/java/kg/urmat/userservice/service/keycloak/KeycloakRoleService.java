package kg.urmat.userservice.service.keycloak;

import org.keycloak.representations.idm.RoleRepresentation;

public interface KeycloakRoleService {

    RoleRepresentation getByName(String roleCode);
}
