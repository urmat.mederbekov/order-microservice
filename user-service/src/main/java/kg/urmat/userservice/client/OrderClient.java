package kg.urmat.userservice.client;

import kg.urmat.order_lib.dto.order.OrderResponse;
import kg.urmat.order_lib.util.contstants.Route;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "order-service", path = Route.ORDERS_API)
public interface OrderClient {

    @GetMapping("/by-user/{authId}")
    List<OrderResponse> getAllByUserId(@PathVariable String authId);
}
