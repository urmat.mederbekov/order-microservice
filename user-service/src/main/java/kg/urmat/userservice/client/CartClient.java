package kg.urmat.userservice.client;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import kg.urmat.order_lib.dto.cart.CartResponse;
import kg.urmat.order_lib.util.contstants.Route;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@Valid
@FeignClient(name = "cart-service", path = Route.CARTS_API)@NotNull
public interface CartClient {

    @GetMapping
    void add(@RequestParam String authId);

    @GetMapping("/by-user/{authId}")
    CartResponse getByUserId(@PathVariable String authId);
}
