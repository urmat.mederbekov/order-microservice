package kg.urmat.userservice.repository;

import kg.urmat.userservice.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByAuthId(String authId);

    Optional<User> findByUsername(String username);

    boolean existsByUsername(String username);
}
