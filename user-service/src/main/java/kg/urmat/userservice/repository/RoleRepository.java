package kg.urmat.userservice.repository;

import kg.urmat.userservice.model.Role;
import kg.urmat.order_lib.util.enums.RoleCode;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Optional<Role> findByCode(RoleCode code);

    Set<Role> findByCodeIn(Collection<RoleCode> codes);
}
