package kg.urmat.userservice.facade.impl;

import kg.urmat.order_lib.dto.user.UserCreateRequest;
import kg.urmat.order_lib.dto.user.UserResponse;
import kg.urmat.order_lib.dto.user.UserUpdateRequest;
import kg.urmat.order_lib.util.enums.RoleCode;
import kg.urmat.userservice.client.CartClient;
import kg.urmat.userservice.facade.UserFacade;
import kg.urmat.userservice.mapper.KeycloakUserMapper;
import kg.urmat.userservice.mapper.UserMapper;
import kg.urmat.userservice.model.Role;
import kg.urmat.userservice.model.User;
import kg.urmat.userservice.service.RoleService;
import kg.urmat.userservice.service.UserService;
import kg.urmat.userservice.service.keycloak.KeycloakRoleService;
import kg.urmat.userservice.service.keycloak.KeycloakUserService;
import lombok.RequiredArgsConstructor;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserFacadeImpl implements UserFacade {

    private final UserService userService;
    private final UserMapper userMapper;
    private final RoleService roleService;
    private final CartClient cartClient;
    private final KeycloakUserService keycloakUserService;
    private final KeycloakRoleService keycloakRoleService;
    private final KeycloakUserMapper keycloakUserMapper;

    @Override
    public UserResponse get(String authId) {
        return userMapper.toResponsePublic(userService.get(authId));
    }

    @Override
    public UserResponse add(UserCreateRequest request) {

        if (keycloakUserService.add(keycloakUserMapper.toEntity(request)) == 201){

            List<UserRepresentation> keycloakUsers = keycloakUserService.getByUsername(request.username());
            List<RoleRepresentation> keycloakRoles = request.roles()
                    .stream()
                    .map(roleCode -> keycloakRoleService.getByName(roleCode.name()))
                    .collect(Collectors.toList());

            keycloakUserService.assignRoles(keycloakUsers.get(0).getId(), keycloakRoles);
            Set<Role> roles = new HashSet<>(Collections.singleton(roleService.getByCode(RoleCode.USER)));
            User user = userService.save(userMapper.toEntity(request, keycloakUsers.get(0).getId(), roles));
            if (user.getRoles().stream().map(Role::getCode).anyMatch(code -> code.equals(RoleCode.USER))){
                cartClient.add(user.getAuthId());
            }
            return userMapper.toResponsePublic(user);
        }
        return null;
    }

    @Override
    public UserResponse update(UserUpdateRequest request, String authId) {

        User user = userService.get(authId);
        Set<Role> roles = roleService.getAllByCodes(request.roleCodes());
        user = userMapper.toEntity(request, user, roles);
        keycloakUserService.update(keycloakUserMapper.toEntity(request, user.getAuthId()));

        UserRepresentation keycloakUser = keycloakUserService.getById(user.getAuthId());
        List<RoleRepresentation> keycloakRoles = request.roleCodes()
                .stream()
                .map(roleCode -> keycloakRoleService.getByName(roleCode.name()))
                .collect(Collectors.toList());

        keycloakUserService.assignRoles(keycloakUser.getId(), keycloakRoles);
        if (user.getRoles().stream().map(Role::getCode).anyMatch(code -> code.equals(RoleCode.USER))){
            cartClient.add(user.getAuthId());
        }
        return userMapper.toResponsePublic(userService.save(user));
    }
}
