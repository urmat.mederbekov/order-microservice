package kg.urmat.userservice.facade;

import kg.urmat.order_lib.dto.order.OrderResponse;

import java.util.List;

public interface OrderFacade {

    List<OrderResponse> getAllByUserId(String userId);
}
