package kg.urmat.userservice.facade.impl;

import kg.urmat.order_lib.dto.cart.CartResponse;
import kg.urmat.userservice.client.CartClient;
import kg.urmat.userservice.facade.CartFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CartFacadeImpl implements CartFacade {

    private final CartClient client;

    @Override
    public CartResponse getByUserId(String userId) {
        return client.getByUserId(userId);
    }
}
