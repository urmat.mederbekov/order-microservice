package kg.urmat.userservice.facade;

import kg.urmat.order_lib.dto.cart.CartResponse;

public interface CartFacade {

    CartResponse getByUserId(String userId);
}
