package kg.urmat.userservice.facade.impl;

import kg.urmat.order_lib.dto.order.OrderResponse;
import kg.urmat.userservice.client.OrderClient;
import kg.urmat.userservice.facade.OrderFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderFacadeImpl implements OrderFacade {

    private final OrderClient client;

    @Override
    public List<OrderResponse> getAllByUserId(String userId) {
        return client.getAllByUserId(userId);
    }
}
