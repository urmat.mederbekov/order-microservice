package kg.urmat.userservice.facade;

import jakarta.validation.Valid;
import kg.urmat.order_lib.dto.user.UserCreateRequest;
import kg.urmat.order_lib.dto.user.UserResponse;
import kg.urmat.order_lib.dto.user.UserUpdateRequest;
import org.springframework.validation.annotation.Validated;

@Validated
public interface UserFacade {

    UserResponse get(String authId);

    UserResponse add(@Valid UserCreateRequest request);

    UserResponse update(@Valid UserUpdateRequest request, String authId);
}
