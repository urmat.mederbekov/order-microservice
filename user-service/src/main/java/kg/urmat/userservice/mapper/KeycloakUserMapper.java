package kg.urmat.userservice.mapper;

import kg.urmat.order_lib.dto.user.UserCreateRequest;
import kg.urmat.order_lib.dto.user.UserUpdateRequest;
import org.keycloak.representations.idm.UserRepresentation;

public interface KeycloakUserMapper {

    UserRepresentation toEntity(UserCreateRequest request);

    UserRepresentation toEntity(UserUpdateRequest request, String id);

}
