package kg.urmat.userservice.mapper;

import kg.urmat.userservice.model.Role;
import kg.urmat.userservice.model.User;
import kg.urmat.order_lib.dto.user.UserCreateRequest;
import kg.urmat.order_lib.dto.user.UserResponse;
import kg.urmat.order_lib.dto.user.UserUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import java.util.Set;

import static org.mapstruct.CollectionMappingStrategy.TARGET_IMMUTABLE;
import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING, uses = RoleMapper.class, collectionMappingStrategy = TARGET_IMMUTABLE)
public interface UserMapper {

    UserResponse toResponsePublic(User user);

    User toEntity(UserCreateRequest request, String authId, Set<Role> roles);

    User toEntity(UserUpdateRequest request, @MappingTarget User user, Set<Role> roles);
}
