package kg.urmat.userservice.mapper;

import kg.urmat.userservice.model.Role;
import kg.urmat.order_lib.dto.role.RoleResponse;
import org.mapstruct.Mapper;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface RoleMapper {

    RoleResponse toResponsePubic(Role role);
}
