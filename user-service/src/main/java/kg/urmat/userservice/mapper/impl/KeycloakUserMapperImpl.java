package kg.urmat.userservice.mapper.impl;

import kg.urmat.order_lib.dto.user.UserCreateRequest;
import kg.urmat.order_lib.dto.user.UserUpdateRequest;
import kg.urmat.userservice.mapper.KeycloakUserMapper;
import kg.urmat.userservice.service.keycloak.KeycloakUserService;
import lombok.RequiredArgsConstructor;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
@RequiredArgsConstructor
public class KeycloakUserMapperImpl implements KeycloakUserMapper {

    private final KeycloakUserService keycloakUserService;

    @Override
    public UserRepresentation toEntity(UserCreateRequest request) {
        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setValue(request.password());
        credential.setTemporary(false);

        UserRepresentation user = new UserRepresentation();
        user.setUsername(request.username());
        user.setCredentials(Collections.singletonList(credential));
        user.setFirstName(request.name());
        user.setLastName(request.surname());
        user.setEnabled(true);

        return user;
    }

    @Override
    public UserRepresentation toEntity(UserUpdateRequest request, String id) {
        UserRepresentation user = keycloakUserService.getById(id);
        user.setFirstName(request.name());
        user.setLastName(request.surname());
        return user;
    }
}
