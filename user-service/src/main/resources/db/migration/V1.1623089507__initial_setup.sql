create table if not exists users
(
    id                       bigserial primary key,
    auth_id                  varchar(100) not null unique,
    username                 varchar(16)  not null unique,
    name                     varchar(100) not null,
    surname                  varchar(100) not null,
    birth_date               date,
    enabled                  boolean default true
    );

create table if not exists roles
(
    id                       bigserial primary key,
    code                     varchar(100) not null unique,
    name                     varchar(100) not null
    );

create table if not exists m2m_user_role
(
    user_id                  bigint references users(id) NOT NULL,
    role_id                  bigint references roles(id) NOT NULL
    );