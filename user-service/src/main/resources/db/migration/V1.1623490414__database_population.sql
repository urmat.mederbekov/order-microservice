INSERT INTO roles (code, name)
VALUES ('ADMIN', 'Админ'),
       ('USER', 'Пользователь')
    ON CONFLICT DO NOTHING;

INSERT INTO users (auth_id, username, name, surname, birth_date)
VALUES ('1ea0568c-4024-49b6-b415-9d8acd35d523', 'admin', 'Urmat', 'Mederbekov', '2001-02-23')
    ON CONFLICT DO NOTHING;


INSERT INTO m2m_user_role
SELECT (SELECT id FROM users WHERE username='admin'), (SELECT id FROM roles where code='ADMIN')
    WHERE NOT EXISTS(
    SELECT * FROM m2m_user_role
    WHERE user_id = (SELECT id FROM users WHERE username='admin')
      and role_id = (SELECT id FROM roles where code='ADMIN')
    );
