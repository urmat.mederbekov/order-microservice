package kg.urmat.productservice.mapper;

import kg.urmat.productservice.model.Product;
import kg.urmat.order_lib.dto.product.ProductCreateRequest;
import kg.urmat.order_lib.dto.product.ProductResponse;
import kg.urmat.order_lib.dto.product.ProductUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface ProductMapper {

    ProductResponse toResponsePublic(Product product);

    Product toEntity(ProductCreateRequest request);

    Product toEntity(ProductUpdateRequest request, @MappingTarget Product product);
}
