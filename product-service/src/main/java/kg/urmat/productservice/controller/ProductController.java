package kg.urmat.productservice.controller;

import kg.urmat.productservice.facade.ProductFacade;
import kg.urmat.order_lib.dto.product.ProductCreateRequest;
import kg.urmat.order_lib.dto.product.ProductResponse;
import kg.urmat.order_lib.dto.product.ProductUpdateRequest;
import kg.urmat.order_lib.util.contstants.RoleConstant;
import kg.urmat.order_lib.util.contstants.Route;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Route.PRODUCTS_API)
@RequiredArgsConstructor
public class ProductController {

    private final ProductFacade productFacade;

    @GetMapping("/{id}")
    public ProductResponse get(@PathVariable Long id) {
        return productFacade.get(id);
    }

    @Secured(RoleConstant.ADMIN)
    @PostMapping
    public ProductResponse create(@RequestBody ProductCreateRequest request){
        return productFacade.add(request);
    }

    @PutMapping("/{id}")
    public ProductResponse update(@RequestBody ProductUpdateRequest request, @PathVariable Long id){
        return productFacade.update(request, id);
    }
}
