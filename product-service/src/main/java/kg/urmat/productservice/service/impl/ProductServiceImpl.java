package kg.urmat.productservice.service.impl;

import kg.urmat.productservice.model.Product;
import kg.urmat.productservice.repository.ProductRepository;
import kg.urmat.productservice.service.ProductService;
import kg.urmat.order_lib.exception.NoSuchElementFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository repository;

    @Override
    public Product get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NoSuchElementFoundException(String.format("No Product with id %d found", id)));
    }

    @Override
    public Product save(Product product) {
        return repository.save(product);
    }
}
