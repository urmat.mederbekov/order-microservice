package kg.urmat.productservice.service;

import kg.urmat.productservice.model.Product;

public interface ProductService {

    Product get(Long id);

    Product save(Product product);
}
