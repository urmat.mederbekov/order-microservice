package kg.urmat.productservice.model;

import jakarta.persistence.*;
import kg.urmat.order_lib.util.enums.ProductCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, unique = true)
    private ProductCode code;

    @Column(nullable = false)
    private String name;

    private String description;

    private double price;
}
