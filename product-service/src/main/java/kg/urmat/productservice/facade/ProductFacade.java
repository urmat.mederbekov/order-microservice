package kg.urmat.productservice.facade;

import jakarta.validation.Valid;
import kg.urmat.order_lib.dto.product.ProductCreateRequest;
import kg.urmat.order_lib.dto.product.ProductResponse;
import kg.urmat.order_lib.dto.product.ProductUpdateRequest;
import org.springframework.validation.annotation.Validated;

@Validated
public interface ProductFacade {

    ProductResponse get(Long id);

    ProductResponse add(@Valid ProductCreateRequest request);

    ProductResponse update(@Valid ProductUpdateRequest request, Long productId);
}
