package kg.urmat.productservice.facade.impl;

import kg.urmat.productservice.facade.ProductFacade;
import kg.urmat.productservice.mapper.ProductMapper;
import kg.urmat.productservice.model.Product;
import kg.urmat.productservice.service.ProductService;
import kg.urmat.order_lib.dto.product.ProductCreateRequest;
import kg.urmat.order_lib.dto.product.ProductResponse;
import kg.urmat.order_lib.dto.product.ProductUpdateRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductFacadeImpl implements ProductFacade {

    private final ProductService productService;
    private final ProductMapper productMapper;

    @Override
    public ProductResponse get(Long id) {
        return productMapper.toResponsePublic(productService.get(id));
    }

    @Override
    public ProductResponse add(ProductCreateRequest request) {
        Product product = productService.save(productMapper.toEntity(request));
        return productMapper.toResponsePublic(product);
    }

    @Override
    public ProductResponse update(ProductUpdateRequest request, Long productId) {
        Product product = productService.save(productMapper.toEntity(request, productService.get(productId)));
        return productMapper.toResponsePublic(product);
    }
}
