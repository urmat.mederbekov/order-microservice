create table if not exists products
(
    id                       bigserial primary key,
    code                     varchar(100) not null,
    name                     varchar(100) not null,
    description              varchar(500),
    price                    double precision
    );