package kg.urmat.orderservice.controller;

import kg.urmat.orderservice.facade.OrderFacade;
import kg.urmat.order_lib.dto.order.OrderResponse;
import kg.urmat.order_lib.util.contstants.RoleConstant;
import kg.urmat.order_lib.util.contstants.Route;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Route.ORDERS_API)
@RequiredArgsConstructor
public class OrderController {

    private final OrderFacade orderFacade;

    @GetMapping("/{id}")
    public OrderResponse get(@PathVariable Long id){
        return orderFacade.get(id);
    }

    @PreAuthorize("@principalContextProviderImpl.isAllowed(#authId)")
    @GetMapping("/by-user/{authId}")
    public List<OrderResponse> getAllByUserId(@PathVariable String authId){
        return orderFacade.getAllByUserId(authId);
    }

    @Secured(RoleConstant.USER)
    @PostMapping
    public OrderResponse create(){
        return orderFacade.add();
    }
}
