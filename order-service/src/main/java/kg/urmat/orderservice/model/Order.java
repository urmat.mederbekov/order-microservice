package kg.urmat.orderservice.model;

import jakarta.persistence.*;
import kg.urmat.order_lib.util.enums.StatusCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, unique = true)
    private StatusCode status = StatusCode.PROCESSING;

    private double totalCost;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<OrderItem> orderItems;

    private String userId;
}
