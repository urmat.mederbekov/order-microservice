package kg.urmat.orderservice.service;

import kg.urmat.orderservice.model.Order;

import java.util.List;

public interface OrderService {

    Order get(Long id);

    List<Order> getAllByUserId(String userId);

    Order save(Order order);
}
