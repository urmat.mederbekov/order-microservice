package kg.urmat.orderservice.service.impl;

import kg.urmat.orderservice.model.Order;
import kg.urmat.orderservice.repository.OrderRepository;
import kg.urmat.orderservice.service.OrderService;
import kg.urmat.order_lib.exception.NoSuchElementFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository repository;

    @Override
    public Order get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NoSuchElementFoundException(String.format("No Order with id %d is found", id)));
    }

    @Override
    public List<Order> getAllByUserId(String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    public Order save(Order order) {
        return repository.save(order);
    }
}
