package kg.urmat.orderservice.service.auth;

public interface PrincipalContextProvider {

    String getPrincipalId();

    boolean isAllowed(String authId);

    String getToken();
}
