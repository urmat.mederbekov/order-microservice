package kg.urmat.orderservice.service.auth.impl;

import kg.urmat.orderservice.service.auth.PrincipalContextProvider;
import kg.urmat.order_lib.util.contstants.RoleConstant;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
public class PrincipalContextProviderImpl implements PrincipalContextProvider {

    @Override
    public String getPrincipalId() {
        return (String) Objects.requireNonNull(getTokenAttributes()).get("sub");
    }

    @Override
    public boolean isAllowed(String authId){
        return Objects.requireNonNull(getAuthorities()).contains(RoleConstant.ADMIN) || getPrincipalId().equals(authId);
    }

    @Override
    public String getToken(){
        return Optional.ofNullable(getAuthentication())
                .map(authentication -> authentication.getToken().getTokenValue())
                .orElse(null);
    }

    private List<String> getAuthorities() {
        return Optional.ofNullable(getAuthentication())
                .map(authentication -> authentication.getAuthorities().stream()
                        .map(GrantedAuthority::getAuthority)
                        .collect(Collectors.toList())
                ).orElse(Collections.emptyList());
    }

    private Map<String, Object> getTokenAttributes(){
        return Optional.ofNullable(getAuthentication())
                .map(JwtAuthenticationToken::getTokenAttributes)
                .orElse(null);
    }

    private JwtAuthenticationToken getAuthentication(){
        JwtAuthenticationToken authentication = (JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        return isAuthenticated(authentication) ? authentication : null;
    }

    private boolean isAuthenticated(Authentication authentication) {
        return nonNull(authentication) && authentication.isAuthenticated() &&
                !AnonymousAuthenticationToken.class.isAssignableFrom(authentication.getClass());
    }
}
