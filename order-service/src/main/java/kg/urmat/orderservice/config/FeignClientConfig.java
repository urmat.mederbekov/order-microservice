package kg.urmat.orderservice.config;

import feign.RequestInterceptor;
import kg.urmat.orderservice.service.auth.PrincipalContextProvider;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.springframework.cloud.openfeign.security.OAuth2AccessTokenInterceptor.AUTHORIZATION;
import static org.springframework.cloud.openfeign.security.OAuth2AccessTokenInterceptor.BEARER;

@Configuration
@AllArgsConstructor
public class FeignClientConfig {

    private final PrincipalContextProvider principalContextProvider;

    @Bean
    public RequestInterceptor requestInterceptor() {
        return requestTemplate -> requestTemplate.header(
                AUTHORIZATION, String.format("%s %s", BEARER, principalContextProvider.getToken())
        );
    }
}
