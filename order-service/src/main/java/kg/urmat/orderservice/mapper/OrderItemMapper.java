package kg.urmat.orderservice.mapper;

import kg.urmat.orderservice.model.OrderItem;
import kg.urmat.order_lib.dto.cart_item.CartItemResponse;
import kg.urmat.order_lib.dto.order_item.OrderItemResponse;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface OrderItemMapper {

    @Mapping(target = "orderId", source = "order.id")
    OrderItemResponse toResponsePublic(OrderItem orderItem);

    @IterableMapping(qualifiedByName = "mapWithoutId")
    List<OrderItem> toEntities(List<CartItemResponse> cartItems);

    @Named("mapWithoutId")
    @Mapping(target = "id", ignore = true)
    OrderItem mapWithoutId(CartItemResponse source);
}
