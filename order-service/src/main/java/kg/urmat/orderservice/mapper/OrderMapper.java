package kg.urmat.orderservice.mapper;

import kg.urmat.orderservice.model.Order;
import kg.urmat.order_lib.dto.cart.CartResponse;
import kg.urmat.order_lib.dto.order.OrderResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING, uses = OrderItemMapper.class)
public interface OrderMapper {

    OrderResponse toResponsePublic(Order order);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "orderItems", source = "items")
    Order toEntity(CartResponse cart);

}
