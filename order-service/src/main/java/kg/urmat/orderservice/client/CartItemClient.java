package kg.urmat.orderservice.client;

import kg.urmat.order_lib.util.contstants.Route;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "cart-service", contextId = "cart-items", path = Route.CART_ITEMS_API)
public interface CartItemClient {

    @DeleteMapping
    void emptyCart(@RequestParam List<Long> cartItemIds);
}
