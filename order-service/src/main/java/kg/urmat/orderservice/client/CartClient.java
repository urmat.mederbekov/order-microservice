package kg.urmat.orderservice.client;

import kg.urmat.order_lib.dto.cart.CartResponse;
import kg.urmat.order_lib.util.contstants.Route;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cart-service", contextId = "carts", path = Route.CARTS_API)
public interface CartClient {

    @GetMapping("/by-user/{authId}")
    CartResponse getByUserId(@PathVariable String authId);
}
