package kg.urmat.orderservice.client;

import kg.urmat.order_lib.dto.product.ProductResponse;
import kg.urmat.order_lib.util.contstants.Route;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "product-service", path = Route.PRODUCTS_API)
public interface ProductClient {

    @GetMapping("/{id}")
    ProductResponse get(@PathVariable Long id);
}
