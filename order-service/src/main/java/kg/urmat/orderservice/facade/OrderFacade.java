package kg.urmat.orderservice.facade;

import kg.urmat.order_lib.dto.order.OrderResponse;

import java.util.List;

public interface OrderFacade {

    OrderResponse get(Long id);

    List<OrderResponse> getAllByUserId(String userId);

    OrderResponse add();
}
