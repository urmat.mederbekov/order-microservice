package kg.urmat.orderservice.facade.impl;

import kg.urmat.order_lib.dto.cart.CartResponse;
import kg.urmat.order_lib.dto.cart_item.CartItemResponse;
import kg.urmat.order_lib.dto.order.OrderResponse;
import kg.urmat.orderservice.client.CartClient;
import kg.urmat.orderservice.client.CartItemClient;
import kg.urmat.orderservice.facade.OrderFacade;
import kg.urmat.orderservice.mapper.OrderMapper;
import kg.urmat.orderservice.model.Order;
import kg.urmat.orderservice.service.OrderService;
import kg.urmat.orderservice.service.auth.PrincipalContextProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderFacadeImpl implements OrderFacade {

    private final PrincipalContextProvider principalContextProvider;
    private final OrderService orderService;
    private final CartClient cartClient;
    private final CartItemClient cartItemClient;
    private final OrderMapper orderMapper;

    @Override
    public OrderResponse get(Long id) {
        return orderMapper.toResponsePublic(orderService.get(id));
    }

    @Override
    public List<OrderResponse> getAllByUserId(String userId){
        return orderService.getAllByUserId(userId).stream().map(orderMapper::toResponsePublic).collect(Collectors.toList());
    }

    @Override
    public OrderResponse add() {
        String userId = principalContextProvider.getPrincipalId();
        CartResponse cart = cartClient.getByUserId(userId);
        Order order = orderMapper.toEntity(cart);
        cartItemClient.emptyCart(cart.items().stream().map(CartItemResponse::id).collect(Collectors.toList()));
        return orderMapper.toResponsePublic(orderService.save(order));
    }
}
