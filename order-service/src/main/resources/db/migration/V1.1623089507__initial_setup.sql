create table if not exists orders
(
    id                       bigserial primary key,
    status                   varchar(100) NOT NULL,
    total_cost               double precision,
    user_id                  varchar(100) NOT NULL
    );

create table if not exists m2m_order_items
(
    id                       bigserial primary key,
    order_id                 bigint references orders(id) NOT NULL,
    product_id               bigint NOT NULL,
    quantity                 integer
    );