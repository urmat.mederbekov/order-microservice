# Order Microservice
Ecommerce App
### Requirements

* Java version 17
* Spring Boot version 3.1.3
* Postgres 12

### Features
* Centralized Configuration Server
* Centralized Authorization Server (Keycloak) with Oauth2
* Discovery Server
* Gateway Routing
* Rest Based Calls via Feign
* Role Based Authorization
* N-layer Architecture (Controller, Facade, Mapper, Service, Repository)
* Custom Validating Annotations
* Multistage Docker Build (Generate, Extract, Run)

### Deploy

```
git clone https://gitlab.com/urmat.mederbekov/order-microservice.git
```
Run `keycloak-postgres.yml` compose file.
```
docker-compose -f keycloak-postgres.yml up
```
Open http://localhost:8080/auth and login as user 'admin' with password 'admin'.

Regenerate **secret keys** for each protected service.
___
![](images\1.PNG)
___
 Add **new user** 'admin', set password, and assign 'ADMIN' role to him.
___
![](images\2.PNG)
___
![](images\3.PNG)
___

Update **passwords** in `.env` file with **regenerated keys** and update **authId** with admin id in keycloak server in `V1.1623490414__database_population.sql` file in **user-service** application.
___
![](images\4.PNG)
___
![](images\5.PNG)
___

Run `docker-compose.yml` compose file.
```
docker-compose up
```
### HOW TO USE
All services are protected by keycloak. In order to access them, you need `access token`. Postman can be used to get token.
___
![](images\6.PNG)
___
All services are accessible through `gateway port: 8081`
___
![](images\7.PNG)
___

### Notes
Admin can create a user with any role(s)

Only user with role USER can order:

User

1) fills his cart with cart items
2) orders (his cart items converted to order items, the cart is emptied and order is created)

## Author

* https://gitlab.com/urmat.mederbekov
