package kg.urmat.cartservice.mapper;

import kg.urmat.cartservice.model.CartItem;
import kg.urmat.order_lib.dto.cart_item.CartItemCreateRequest;
import kg.urmat.order_lib.dto.cart_item.CartItemResponse;
import kg.urmat.order_lib.dto.cart_item.CartItemUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING)
public interface CartItemMapper {

    @Mapping(target = "cartId", source = "cart.id")
    CartItemResponse toResponsePublic(CartItem entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "quantity", source = "request.quantity")
    CartItem toEntity(CartItemCreateRequest request);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "quantity", source = "request.quantity")
    CartItem toEntity(CartItemUpdateRequest request, @MappingTarget CartItem entity);
}
