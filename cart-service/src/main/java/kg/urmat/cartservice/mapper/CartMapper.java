package kg.urmat.cartservice.mapper;

import kg.urmat.cartservice.model.Cart;
import kg.urmat.order_lib.dto.cart.CartResponse;
import org.mapstruct.Mapper;

import static org.mapstruct.MappingConstants.ComponentModel.SPRING;

@Mapper(componentModel = SPRING, uses = CartItemMapper.class)
public interface CartMapper {

    CartResponse toResponsePublic(Cart entity);
}
