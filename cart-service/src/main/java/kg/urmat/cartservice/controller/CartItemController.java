package kg.urmat.cartservice.controller;

import kg.urmat.cartservice.facade.CartItemFacade;
import kg.urmat.order_lib.dto.cart_item.CartItemCreateRequest;
import kg.urmat.order_lib.dto.cart_item.CartItemResponse;
import kg.urmat.order_lib.dto.cart_item.CartItemUpdateRequest;
import kg.urmat.order_lib.util.contstants.RoleConstant;
import kg.urmat.order_lib.util.contstants.Route;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Route.CART_ITEMS_API)
@Secured(RoleConstant.USER)
@RequiredArgsConstructor
public class CartItemController {

    private final CartItemFacade cartItemFacade;

    @PostMapping
    public CartItemResponse create(@RequestBody CartItemCreateRequest request){
        return cartItemFacade.add(request);
    }

    @PutMapping("/{id}")
    public CartItemResponse update(@RequestBody CartItemUpdateRequest request, @PathVariable Long id){
        return cartItemFacade.update(request, id);
    }

    @DeleteMapping
    public void emptyCart(@RequestParam List<Long> cartItemIds){
        cartItemFacade.emptyCart(cartItemIds);
    }
}
