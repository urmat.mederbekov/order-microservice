package kg.urmat.cartservice.controller;

import kg.urmat.cartservice.facade.CartFacade;
import kg.urmat.order_lib.dto.cart.CartResponse;
import kg.urmat.order_lib.util.contstants.RoleConstant;
import kg.urmat.order_lib.util.contstants.Route;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(Route.CARTS_API)
@RequiredArgsConstructor
public class CartController {

    private final CartFacade cartFacade;

    @Secured(RoleConstant.USER)
    @GetMapping
    public void add(@RequestParam String authId){
        cartFacade.add(authId);
    }

    @PreAuthorize("@principalContextProviderImpl.isAllowed(#authId)")
    @GetMapping("/by-user/{authId}")
    public CartResponse getByUserId(@PathVariable String authId){
        return cartFacade.getByUserId(authId);
    }
}
