package kg.urmat.cartservice.facade;

import jakarta.validation.Valid;
import kg.urmat.order_lib.dto.cart_item.CartItemCreateRequest;
import kg.urmat.order_lib.dto.cart_item.CartItemResponse;
import kg.urmat.order_lib.dto.cart_item.CartItemUpdateRequest;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Validated
public interface CartItemFacade {

    CartItemResponse add(@Valid CartItemCreateRequest request);

    CartItemResponse update(@Valid CartItemUpdateRequest request, Long entityId);

    void emptyCart(List<Long> cartItemIds);
}
