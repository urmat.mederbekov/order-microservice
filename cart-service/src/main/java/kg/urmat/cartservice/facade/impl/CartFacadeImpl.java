package kg.urmat.cartservice.facade.impl;

import kg.urmat.cartservice.facade.CartFacade;
import kg.urmat.cartservice.mapper.CartMapper;
import kg.urmat.cartservice.model.Cart;
import kg.urmat.cartservice.service.CartService;
import kg.urmat.order_lib.dto.cart.CartResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CartFacadeImpl implements CartFacade {

    private final CartService cartService;
    private final CartMapper cartMapper;

    @Override
    public CartResponse add(String userId) {
        if(!cartService.existsByUserId(userId)){
            Cart cart = new Cart(userId);
            return cartMapper.toResponsePublic(cartService.save(cart));
        }else {
            return cartMapper.toResponsePublic(cartService.getByUserId(userId));
        }

    }

    @Override
    public CartResponse getByUserId(String userId) {
        return cartMapper.toResponsePublic(cartService.getByUserId(userId));
    }
}
