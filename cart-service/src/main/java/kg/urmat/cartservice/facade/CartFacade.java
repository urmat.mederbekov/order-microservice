package kg.urmat.cartservice.facade;

import kg.urmat.order_lib.dto.cart.CartResponse;

public interface CartFacade {

    CartResponse add(String authId);

    CartResponse getByUserId(String userId);
}
