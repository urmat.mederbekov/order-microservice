package kg.urmat.cartservice.facade.impl;

import kg.urmat.cartservice.facade.CartItemFacade;
import kg.urmat.cartservice.mapper.CartItemMapper;
import kg.urmat.cartservice.model.CartItem;
import kg.urmat.cartservice.service.CartItemService;
import kg.urmat.order_lib.dto.cart_item.CartItemCreateRequest;
import kg.urmat.order_lib.dto.cart_item.CartItemResponse;
import kg.urmat.order_lib.dto.cart_item.CartItemUpdateRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CartItemFacadeImpl implements CartItemFacade {

    private final CartItemService cartItemService;
    private final CartItemMapper cartItemMapper;

    @Override
    public CartItemResponse add(CartItemCreateRequest request) {
        CartItem cartItem = cartItemService.save(cartItemMapper.toEntity(request));
        return cartItemMapper.toResponsePublic(cartItem);
    }

    @Override
    public CartItemResponse update(CartItemUpdateRequest request, Long entityId) {
        CartItem cartItem = cartItemService.save(cartItemMapper.toEntity(request, cartItemService.get(entityId)));
        return cartItemMapper.toResponsePublic(cartItem);
    }

    @Override
    public void emptyCart(List<Long> cartItemIds){
        cartItemService.emptyCart(cartItemIds);
    }
}
