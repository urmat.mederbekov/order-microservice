package kg.urmat.cartservice.service.auth;

public interface PrincipalContextProvider {

    String getPrincipalId();

    boolean isAllowed(String authId);

    String getToken();
}
