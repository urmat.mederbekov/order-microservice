package kg.urmat.cartservice.service.impl;

import kg.urmat.cartservice.model.CartItem;
import kg.urmat.cartservice.repository.CartItemRepository;
import kg.urmat.cartservice.service.CartItemService;
import kg.urmat.order_lib.exception.NoSuchElementFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CartItemServiceImpl implements CartItemService {

    private final CartItemRepository repository;

    @Override
    public CartItem get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NoSuchElementFoundException(String.format("No CartItem with id %d found", id)));
    }

    @Override
    public CartItem save(CartItem entity) {
        return repository.save(entity);
    }

    @Override
    public void emptyCart(List<Long> cartItemIds) {
        repository.deleteAllByIdInBatch(cartItemIds);
    }

    public boolean existsInCart(Long cartId, Long productId){return repository.existsByCartIdAndProductId(cartId, productId);}
}
