package kg.urmat.cartservice.service;

import kg.urmat.cartservice.model.Cart;

public interface CartService {

    Cart get(Long id);

    Cart getByUserId(String userId);

    Cart save(Cart cart);

    boolean existsByUserId(String userId);
}
