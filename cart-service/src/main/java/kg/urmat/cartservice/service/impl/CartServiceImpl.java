package kg.urmat.cartservice.service.impl;

import kg.urmat.cartservice.model.Cart;
import kg.urmat.cartservice.repository.CartRepository;
import kg.urmat.cartservice.service.CartService;
import kg.urmat.order_lib.exception.NoSuchElementFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CartServiceImpl implements CartService {

    private final CartRepository repository;

    @Override
    public Cart get(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new NoSuchElementFoundException(String.format("No Cart with id %d found", id)));
    }

    @Override
    public Cart getByUserId(String userId) {
        return repository.findByUserId(userId)
                .orElseThrow(() -> new NoSuchElementFoundException(String.format("No Cart found, whose User id %s", userId)));
    }

    @Override
    public Cart save(Cart cart){
        return repository.save(cart);
    }

    @Override
    public boolean existsByUserId(String userId){
        return repository.existsByUserId(userId);
    }
}
