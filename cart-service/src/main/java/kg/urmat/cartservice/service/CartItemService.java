package kg.urmat.cartservice.service;

import kg.urmat.cartservice.model.CartItem;

import java.util.List;

public interface CartItemService {

    CartItem get(Long id);

    CartItem save(CartItem entity);

    void emptyCart(List<Long> cartItemIds);

    boolean existsInCart(Long cartId, Long productId);
}
