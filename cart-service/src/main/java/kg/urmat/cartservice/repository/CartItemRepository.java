package kg.urmat.cartservice.repository;

import kg.urmat.cartservice.model.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartItemRepository extends JpaRepository<CartItem, Long> {

    boolean existsByCartIdAndProductId(Long cartId, Long productId);

}
