package kg.urmat.cartservice.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "carts")
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "cart",fetch = FetchType.EAGER)
    private List<CartItem> items;

    @Column(name = "user_id")
    private String userId;

    public Cart(String userId){
        this.userId = userId;
    }
}
