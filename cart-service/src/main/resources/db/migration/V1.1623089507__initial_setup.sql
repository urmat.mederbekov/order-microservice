create table if not exists carts
(
    id                       bigserial primary key,
    user_id                  varchar(100) NOT NULL
    );

create table if not exists m2m_cart_items
(
    id                       bigserial primary key,
    product_id               bigint NOT NULL,
    quantity                 integer ,
    cart_id                  bigint references carts(id) NOT NULL
    );

